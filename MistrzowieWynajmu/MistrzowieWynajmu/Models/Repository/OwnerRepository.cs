﻿using MistrzowieWynajmu.Models.Database;
using MistrzowieWynajmu.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MistrzowieWynajmu.Models.Repository
{
    public class OwnerRepository : IOwnerRepository
    {
        private readonly DatebaseContext _dataBaseContex;
        public OwnerRepository(DatebaseContext dataBaseContex)
        {
            _dataBaseContex = dataBaseContex;
        }
        public int AddOwner(Owner owner)
        {
            if (owner == null)
            {
                throw new Exception("Owner object cannot be null.");
            }

            _dataBaseContex.Owners.Add(owner);
            _dataBaseContex.SaveChanges();

            return owner.OwnerId;
        }

        public Owner GetOwner(int ownerId)
        {
            if (ownerId <= 0)
            {
                throw new Exception("Id cannot be less than 0.");
            }

            return _dataBaseContex.Owners.FirstOrDefault(owner => owner.OwnerId == ownerId);
        }
    }
}
