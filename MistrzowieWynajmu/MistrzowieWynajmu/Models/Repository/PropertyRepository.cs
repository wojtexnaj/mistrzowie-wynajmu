﻿using MistrzowieWynajmu.Models.Database;
using MistrzowieWynajmu.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MistrzowieWynajmu.Models.Repository
{
    public class PropertyRepository : IPropertyRepository
    {
        private readonly DatebaseContext _dataBaseContex;

        public PropertyRepository(DatebaseContext dataBaseContex)
        {
            _dataBaseContex = dataBaseContex;
        }

        public int AddProperty(Property property, Address address, Owner owner)
        {
            if(property == null)
            {
                throw new Exception("Property object cannot be null.");
            }

            if (address == null)
            {
                throw new Exception("Address object cannot be null.");
            }

            if (owner == null)
            {
                throw new Exception("Owner object cannot be null.");
            }

            property.Id = 0;
            property.Owner = owner;
            property.OwnerId = owner.OwnerId;

            property.Address = address;
            property.AddressId = address.AddressId;

            _dataBaseContex.Properties.Add(property);
            _dataBaseContex.SaveChanges();

            return property.Id;

        }

        public void DeleteProperty(Property property, Address address, Owner owner)
        {
            if (property == null)
            {
                throw new Exception("Property object cannot be null.");
            }

            if (address == null)
            {
                throw new Exception("Address object cannot be null.");
            }

            if (owner == null)
            {
                throw new Exception("Owner object cannot be null.");
            }

            _dataBaseContex.Properties.Remove(property);
            _dataBaseContex.SaveChanges();

            _dataBaseContex.Addresses.Remove(address);
            _dataBaseContex.SaveChanges();

            _dataBaseContex.Owners.Remove(owner);
            _dataBaseContex.SaveChanges();
        }

        public List<Property> GetProperties()
        {
            return _dataBaseContex.Properties.ToList();
        }

        public Property GetProperty(int propertyId)
        {
            if(propertyId <= 0 )
            {
                throw new Exception("Id cannot be less than 0.");
            }

            return _dataBaseContex.Properties.
                Where(property => property.Id == propertyId).FirstOrDefault();
        }

        public int UpdateProperty(Property property)
        {
            if (property == null)
            {
                throw new Exception("Property object cannot be null.");
            }

            _dataBaseContex.Properties.Update(property);
            _dataBaseContex.SaveChanges();

            return property.Id;
        }
    }
}
