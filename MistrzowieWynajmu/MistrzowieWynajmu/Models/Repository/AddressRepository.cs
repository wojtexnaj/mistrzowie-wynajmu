﻿using MistrzowieWynajmu.Models.Database;
using MistrzowieWynajmu.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MistrzowieWynajmu.Models.Repository
{
    public class AddressRepository : IAddressRepository
    {
        private readonly DatebaseContext _datebaseContext;
        public AddressRepository(DatebaseContext datebaseContext)
        {
            _datebaseContext = datebaseContext;
        }
        public int AddAddress(Address address)
        {
            if (address == null)
            {
                throw new Exception("Address object cannot be null.");
            }

            _datebaseContext.Addresses.Add(address);
            _datebaseContext.SaveChanges();

            return address.AddressId;
        }

        public Address GetAddress(int addressId)
        {
            if (addressId <= 0)
            {
                throw new Exception("Id cannot be less than 0.");
            }

            return _datebaseContext.Addresses.FirstOrDefault(address => address.AddressId == addressId);
        }
    }
}
