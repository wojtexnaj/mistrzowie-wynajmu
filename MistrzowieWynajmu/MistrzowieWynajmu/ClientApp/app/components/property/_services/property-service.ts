﻿import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Property } from '../../../_models';
import { PropertiesBackendService } from '../../../_services/properties-backend.service';

@Injectable()
export class PropertyService {
    constructor(
        private propertiesBackendService: PropertiesBackendService
    ) { }

    addProperty(newProperty: Property): Observable<number> {
        return this.propertiesBackendService.addProperty(newProperty);
    }
    getProperty(propertyId: number): Observable<Property> {
        return this.propertiesBackendService.getProperty(propertyId);
    }
    getProperties(): Observable<Property[]> {
        return this.propertiesBackendService.GetProperties();
    }
    updateProperty(updateProperty: Property): Observable<Property> {
        return this.propertiesBackendService.updateProperty(updateProperty);
    }
    deleteProperty(propertyId: number): Observable<number> {
        return this.propertiesBackendService.deleteProperty(propertyId);
    }
}