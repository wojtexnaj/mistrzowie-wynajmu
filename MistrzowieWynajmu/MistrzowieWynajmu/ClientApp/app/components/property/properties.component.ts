﻿import { Component, OnInit } from '@angular/core';
import { Property } from '../../_models';
import { PropertyService } from './_services/property-service';

@Component({
    templateUrl: './properties.component.html'
})

export class PropertiesComponent implements OnInit {

    test = 'Działa';

    constructor(
        private propertyService: PropertyService
    ) {

    }

    ngOnInit(): void {
        this.propertyService.getProperties()
            .subscribe(
            response => console.log('response: ', response),
            error => console.log('error: ', error)
        );
    }
}