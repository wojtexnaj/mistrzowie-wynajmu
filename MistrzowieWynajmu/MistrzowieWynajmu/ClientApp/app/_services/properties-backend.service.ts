﻿import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Property } from '../_models';

@Injectable()
export abstract class PropertiesBackendService {
    abstract addProperty(newProperty: Property): Observable<number>;
    abstract getProperty(id: number): Observable<Property>;
    abstract GetProperties(): Observable<Property[]>;
    abstract updateProperty(updateProperty: Property): Observable<Property>;
    abstract deleteProperty(id: number): Observable<number>;
}