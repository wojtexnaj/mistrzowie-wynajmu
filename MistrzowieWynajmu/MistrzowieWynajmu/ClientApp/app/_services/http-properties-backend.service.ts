﻿import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Property } from '../_models';
import { PropertiesBackendService } from '../_services/properties-backend.service';
import { Http, RequestOptions, Headers, Response } from '@angular/http';

@Injectable()
export class HttpPropertiesBackendServices extends PropertiesBackendService {
    private addPropertyUrl: string = 'api/property/addproperty';
    private getPropertyUrl: string = 'api/property/getproperty?propertyId=';
    private getPropertiesUrl: string = 'api/property/getproperties';
    private updatePropertyUrl: string = 'api/property/updateproperty';
    private deletePropertyUrl: string = 'api/property/deleteProperty?propertyId=';

    private jsonContentOptions: RequestOptions;

    constructor(
        private http: Http
    ) {
        super()
        let headersJson: Headers = new Headers({
            'Content-Type': 'aplication/json',
        });

        this.jsonContentOptions = new RequestOptions({ headers: headersJson})
    }

    addProperty(newProperty: Property): Observable<number> {
        return this.http.post(this.addPropertyUrl, JSON.stringify(newProperty), this.jsonContentOptions)
            .map(response => response.json() as number);
    }
    getProperty(id: number): Observable<Property> {
        return this.http.get(this.getPropertiesUrl + id, this.jsonContentOptions)
            .map(response => response.json());
    }
    GetProperties(): Observable<Property[]> {
        return this.http.get(this.getPropertiesUrl, this.jsonContentOptions)
            .map(response => response.json());
    }
    updateProperty(updateProperty: Property): Observable<Property> {
        return this.http.post(this.updatePropertyUrl, JSON.stringify(updateProperty), this.jsonContentOptions)
            .map(response => response.json());
    }
    deleteProperty(id: number): Observable<number> {
        return this.http.get(this.deletePropertyUrl + id, this.jsonContentOptions)
            .map(response => response.json() as number);
    }
}